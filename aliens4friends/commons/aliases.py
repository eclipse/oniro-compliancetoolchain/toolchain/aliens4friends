# SPDX-FileCopyrightText: NOI Techpark <info@noi.bz.it>
# SPDX-License-Identifier: Apache-2.0

# These mappings are match aliases. That is, if the matchers find
# something on the key-side of this map, then it gets replaced by
# the value. This is useful, not to overcomplicate matching rules.
ALIASES = {
	"gtk+3": "gtk+3.0",
	"gmmlib": "intel-gmmlib",
	"libpcre2": "pcre2",
	"libusb1": "libusb-1.0",
	"libva-intel": "libva",
	"libxfont2": "libxfont",
	"linux-firmware": "firmware-nonfree",
	"linux-intel": "linux",
	"linux-seco-fslc": "linux",
	"linux-stm32mp": "linux",
	"linux-yocto": "linux",
	"python3": "python3.9",
	"systemd-boot": "systemd",
	"tcl": "tcl8.6",
	"xz": "xz-utils",
	"wpa-supplicant": "wpa",
	"zlib-intel": "zlib",
	"python3-dbus": "dbus-python",
	"libpam": "pam",
}

# Exclude these packages from Debian package searches, since we
# are sure that we will not find anything.
EXCLUSIONS = [
	"freertos-demo",
	"zephyr-philosophers",
	"ltp",
	"libpcre",
	"xserver-xorg",
	"which"
]

SNAPMATCH_404_WORKAROUNDS = {
	"libzstd": {
		"1.5.4+dfsg2-1": "1.5.4+dfsg2-2",
	},
	"gawk": {
		"1:5.2.1-1": "1:5.2.1-2",
	},
	"iptables": {
		"1.8.9-1": "1.8.9-2"
	},
	"pam": {
		"1.5.2-1": "1.5.2-2",
	},
	"readline": {
		"8.2~beta-1": "8.2~beta-2"
	},
	"zlib": {
		"1.03-1": "1:1.3.dfsg+really1.3.1-1"
	}
}