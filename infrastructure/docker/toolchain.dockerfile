# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: 2021 NOI Techpark <p.moser@noi.bz.it>
#
# This script is to run spdx-tools and aliens4friends on the host machine,
# without having to install all dependencies there.
#
# Build the image (from the root directory):
#     docker build -t toolchain -f infrastructure/docker/toolchain.dockerfile .
#
# Test it:
#     docker run -it toolchain a4f help
#     docker run -it toolchain spdxtool
#     docker run -it toolchain scancode --help
#
FROM ubuntu:jammy

ARG GITHUB_BOT_AUTH=

# TODOS
# - Use slim or alpine versions if possible
# - use a multi-stage build and copy only necessary files from spdxtool and scancode
# - cleanup apt-get caches
# - combine layers that should be together with a single RUN command

ARG DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends openjdk-11-jdk-headless bzip2 sudo lzip gpg-agent software-properties-common wget python3-pip python-is-python3 gcc patch xz-utils && \
	add-apt-repository -y ppa:deadsnakes/ppa && apt-get update && \
	apt-get install -y --no-install-recommends python3.8 python3.8-dev python3.8-distutils && \
	apt-get autoremove --purge -y && apt-get clean && rm -rf /var/lib/apt/lists/*

### SPDXTOOL INSTALLATION
# Do not use ENV SPDXTOOL_RELEASE=2.2.5, to leverage the docker build cache
COPY infrastructure/utils/spdxtool-wrapper /usr/local/bin/spdxtool
RUN wget --progress=dot:giga -P /usr/local/lib \
    https://${GITHUB_BOT_AUTH}@github.com/spdx/tools/releases/download/v2.2.5/spdx-tools-2.2.5-jar-with-dependencies.jar && \
	chmod +x /usr/local/bin/spdxtool

### Prepare Python development prerequisites
#
ARG GIT_REF=HEAD
RUN echo ${GIT_REF}
ENV PATH=/code/bin:$PATH
COPY setup.py README.md /code/
COPY bin/* /code/bin/
COPY aliens4friends /code/aliens4friends/

# hadolint ignore=DL3013,DL3003
RUN cd /code && \
	pip3 install --no-cache-dir --upgrade pip && \
	pip3 install --no-cache-dir anybadge && \
	pip3 install --no-cache-dir pytype && \
	pip3 install --no-cache-dir . && \
	python -c "from flanker.addresslib import address" >/dev/null 2>&1

RUN useradd --create-home --uid 1000 --shell /bin/bash a4fuser && \
	usermod -aG sudo a4fuser && \
	echo "a4fuser ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

# workaround to increase fossology upload timeout without increasing polling time
RUN sed -ie 's/stop_after_attempt(10)/stop_after_attempt(30)/' /usr/local/lib/python3.10/dist-packages/fossology/uploads.py

USER a4fuser
SHELL ["/bin/bash", "-c"]

### SCANCODE INSTALLATION
RUN mkdir -p /home/a4fuser/.local/bin
COPY --chown=a4fuser:a4fuser infrastructure/docker/scancode.patch /home/a4fuser
COPY --chown=a4fuser:a4fuser infrastructure/docker/scancode /home/a4fuser/.local/bin
RUN chmod +x /home/a4fuser/.local/bin/scancode
ENV PATH="$PATH:/home/a4fuser/.local/bin"

# hadolint ignore=SC2102,DL3013,DL3003
RUN pip3 install --no-cache-dir --user virtualenv && \
    virtualenv -p /usr/bin/python3.8 /home/a4fuser/scancode-env && \
	cd /home/a4fuser && \
    source scancode-env/bin/activate && \
    pip3 install --no-cache-dir setuptools wheel click==6.7 bitarray==0.8.1 \
      pygments==2.4.2 commoncode==20.10.20 pluggy==0.13.1 \
	  extractcode==20.10 plugincode==20.9 typecode==20.10.20 \
	  dparse2==0.5.0.4  spdx-tools==0.6.1 scancode-toolkit[full]==3.2.3 && \
	cd scancode-env/lib/python3.8/site-packages/scancode && \
    patch -p1 < /home/a4fuser/scancode.patch && \
	cd - && \
    rm scancode.patch && \
	scancode --reindex-licenses && \
    deactivate

CMD [ "bash" ]
